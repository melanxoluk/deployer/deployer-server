import com.github.jengelman.gradle.plugins.shadow.tasks.ShadowJar
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm") version "1.6.10-RC"
    id("com.github.johnrengelman.shadow") version "5.0.0"
}

group = "org.melanxoluk"
version = "0.2"

val ktor = "1.2.3"

repositories {
    mavenCentral()
    jcenter()
}

dependencies {
    implementation(kotlin("stdlib"))
    implementation("org.jetbrains.kotlin:kotlin-reflect:1.4.0")

    implementation("ch.qos.logback:logback-classic:1.2.10")
    implementation("io.github.config4k:config4k:0.4.2")

    implementation("org.jetbrains.exposed:exposed-core:0.26.1")
    implementation("org.jetbrains.exposed:exposed-jdbc:0.26.1")
    implementation("org.jetbrains.exposed:exposed-java-time:0.26.1")
    implementation("org.postgresql:postgresql:42.2.5")

    implementation("io.ktor:ktor-server-netty:$ktor")
    implementation("io.ktor:ktor-jackson:$ktor")

    implementation("com.github.pengrad:java-telegram-bot-api:5.6.0")
    implementation("org.gitlab4j:gitlab4j-api:4.19.0")
    implementation("com.hierynomus:sshj:0.32.0")

    implementation("org.http4k:http4k-core:4.25.5.1")

    testImplementation(kotlin("test"))
}

tasks.withType<Test> {
    useJUnitPlatform()
}

tasks.withType<ShadowJar> {
    manifest {
        attributes["Main-Class"] = "org.melanxoluk.deployer.MainKt"
    }

    archiveBaseName.set("deployer")
    archiveClassifier.set("")
    archiveVersion.set("")
}
