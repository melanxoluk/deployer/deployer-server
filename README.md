todo: replace digit keys with project names


Webhook example
---
http://185.228.233.36:11001/webhooks?key=1


Actual flow
---
- invalidate/add tg channel in database throw sql insert
- add project in db throw sql insert
- navigate to project on gitlab, set webhook accordingly
- test webhook

It would be much easier to use GUI with selecting channels, gitlab projects from comboboxes to make same
flow:
- select/create new tg bot by token & name
- select tg channel from combobox by search (tg login)
- select gitlab project from combobox by search (gitlab login)
- select deploy script for project
- select suggested available port for project

TODO
---
- continue evolution of the Deploy
  - change 'build' on columns to 'start'
  - add reference to script used to start application

Docs
---
Deploy is consist of two components:
1. Prepare files/artifacts on the server
2. Run start command