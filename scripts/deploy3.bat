call gradlew shadowJar -x test
bash -c "scp "$(pwd)/build/libs/deployer.jar" root@185.228.233.36:/root/deployer/deployer.jar"
bash -c "scp "$(pwd)/scripts/restart.sh" root@185.228.233.36:/root/deployer/restart.sh"
bash -c "scp "$(pwd)/scripts/compose.sh" root@185.228.233.36:/root/deployer/compose.sh"
bash -c "scp "$(pwd)/scripts/static_swap.sh" root@185.228.233.36:/root/deployer/static_swap.sh"
bash -c "scp "$(pwd)/scripts/swap.sh" root@185.228.233.36:/root/deployer/swap.sh"
bash -c "scp "$(pwd)/deployer.conf" root@185.228.233.36:/root/deployer/deployer.conf"
bash -c "ssh root@185.228.233.36 sh /root/deployer/restart.sh"
