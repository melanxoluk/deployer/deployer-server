package org.melanxoluk.deployer

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

internal class ProcessesTest {
    @Test
    fun testRun() {
        val results = runProcess(ProcessBuilder("curl", "-I", "https://www.keycdn.com"))
        Assertions.assertTrue(results.exitValue == 0)
    }
}