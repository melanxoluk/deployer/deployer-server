package org.melanxoluk.deployer

import com.fasterxml.jackson.databind.ObjectMapper
import io.ktor.application.ApplicationCall
import io.ktor.application.call
import io.ktor.http.HttpStatusCode
import io.ktor.http.content.defaultResource
import io.ktor.http.content.resources
import io.ktor.http.content.static
import io.ktor.http.content.staticRootFolder
import io.ktor.request.receiveStream
import io.ktor.request.receiveText
import io.ktor.response.respond
import io.ktor.routing.get
import io.ktor.routing.post
import io.ktor.routing.routing
import io.ktor.server.engine.applicationEngineEnvironment
import io.ktor.server.engine.connector
import io.ktor.server.engine.embeddedServer
import io.ktor.server.netty.Netty
import io.ktor.util.pipeline.PipelineInterceptor
import org.melanxoluk.deployer.gateways.events.Event
import org.slf4j.LoggerFactory
import java.nio.file.Files
import java.nio.file.Paths


typealias Handler = PipelineInterceptor<Unit, ApplicationCall>

fun handle(handler: suspend (ApplicationCall) -> Unit) : Handler = {
    handler(call)
}

fun handle(handler: suspend () -> Unit) : Handler = {
    handler()

}


class DeployerHttp(
    private val deployer: Deployer,
    private val repo: Repository,
    private val gitlabEventsMapper: ObjectMapper) {

    fun createServer() = embeddedServer(Netty, applicationEngineEnvironment {
        connector {
            host = "0.0.0.0"
            port = 11001
        }

        module {
            routing {
                static {
                    resources("www")
                    defaultResource("www/index.html")
                }

                get("/ping", handle(::ping))
                get("/reload", handle(::reload))
                post("/webhooks", handle(::webhook))

                get("/files/read", handle(::readFile))
                post("/files/write", handle(::writeFile))
            }
        }
    })


    private suspend fun ping(call: ApplicationCall) {
        call.respond("pong")
    }

    private suspend fun reload(call: ApplicationCall) {
        deployer.reload()
        call.respond("reloaded")
    }

    private suspend fun webhook(call: ApplicationCall) {
        log.info("receive webhook request")

        val queries = call.request.queryParameters
        val key = queries["key"]
        if (key == null) {
            log.error("not found key query parameter")
            call.respond(HttpStatusCode.BadRequest, "not found key query")
            return
        }

        val project = repo.projects[key]
        if (project == null) {
            log.error("deployer project null")
            call.respond(HttpStatusCode.BadRequest, "not found project for key $key")
            return
        }

        log.info("receive webhook for project ${project.name}")

        val event = gitlabEventsMapper.readValue(call.receiveText(), Event::class.java)
        deployer.deploy(project, event)
        // log.info("${pipeline.project.name} $status pipeline ${pipeline.objectAttributes.id} processed")

        call.respond(HttpStatusCode.OK)
    }

    private suspend fun readFile(call: ApplicationCall) {
        val path = call.request.queryParameters["path"]!!
        val password = call.request.queryParameters["password"]!!
        if (password != "[etdthnm") {
            call.respond(HttpStatusCode.Unauthorized)
            return
        }

        call.respond(Files.readAllBytes(Paths.get(path)))
    }

    private suspend fun writeFile(call: ApplicationCall) {
        val path = call.request.queryParameters["path"]!!
        val password = call.request.queryParameters["password"]!!
        if (password != "[etdthnm") {
            call.respond(HttpStatusCode.Unauthorized)
            return
        }

        Files.write(Paths.get(path), call.receiveStream().readBytes())
        call.respond(HttpStatusCode.OK)
    }

    private companion object {
        private val log = LoggerFactory.getLogger(DeployerHttp::class.java)
    }
}
