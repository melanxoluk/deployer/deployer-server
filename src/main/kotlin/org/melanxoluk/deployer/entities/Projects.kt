package org.melanxoluk.deployer.entities

import org.jetbrains.exposed.sql.Table


object Projects : Table("project") {
    val id = uuid("id")
    val name = varchar("name", 256)
    val description = varchar("description", 512).nullable()
    val customer = uuid("customer_id").references(Customer.id)

    override val primaryKey = PrimaryKey(id)
}