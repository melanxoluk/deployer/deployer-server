package org.melanxoluk.deployer.entities

import org.jetbrains.exposed.sql.Table


object TelegramChannels : Table("telegram_channels") {
    val id = uuid("id")
    val name = varchar("name", 256)
    val token = varchar("token", 256)
    val channel = varchar("channel", 256)

    override val primaryKey = PrimaryKey(id)
}
