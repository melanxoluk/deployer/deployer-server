package org.melanxoluk.deployer.entities

import org.jetbrains.exposed.sql.Table
import org.jetbrains.exposed.sql.`java-time`.CurrentDateTime
import org.jetbrains.exposed.sql.`java-time`.datetime

object Deploy: Table("deploy") {
    val id = long("id").autoIncrement("deploy_id_seq")
    val projectId = uuid("deployer_project_id").references(DeployerProjects.id)
    val buildOutput = text("build_output")
    val buildExitValue = integer("build_exit_value")
    val createdAt = datetime("created_at").defaultExpression(CurrentDateTime())
}