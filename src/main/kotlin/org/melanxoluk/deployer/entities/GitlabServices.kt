package org.melanxoluk.deployer.entities

import org.jetbrains.exposed.sql.Table


object GitlabServices : Table("gitlab_services") {
    val id = uuid("id")
    val name = varchar("name", 256)
    val host = varchar("host", 256)
    val token = varchar("token", 256)

    override val primaryKey = PrimaryKey(id)
}
