package org.melanxoluk.deployer.entities

import org.jetbrains.exposed.sql.Table


object DeployerProjects: Table("deployer_projects") {
    val id = uuid("id")
    val name = varchar("name", 256)
    val port = integer("port")
    val path = varchar("path", 512)
    val key = varchar("key", 32)
    val gitlab = uuid("gitlab_id").references(GitlabServices.id)
    val gitlabPath = varchar("gitlab_path", 256)
    val telegram = uuid("telegram_id").references(TelegramChannels.id)
    val deployScript = varchar("deploy_script_path", 256).nullable()

    override val primaryKey = PrimaryKey(id)
}
