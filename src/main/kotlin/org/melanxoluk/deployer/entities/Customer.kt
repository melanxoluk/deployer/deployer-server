package org.melanxoluk.deployer.entities

import org.jetbrains.exposed.sql.Table


object Customer : Table("customer") {
    val id = uuid("id")
    val name = varchar("name", 256)

    override val primaryKey = PrimaryKey(id)
}
