package org.melanxoluk.deployer

import com.fasterxml.jackson.module.kotlin.registerKotlinModule
import org.gitlab4j.api.utils.JacksonJson
import org.slf4j.LoggerFactory


private val log = LoggerFactory.getLogger("deployer")

fun main(args: Array<String>) {
    val repo = Repository().apply { reload() }
    val telegram = Telegram()
    val gitlabEventsMapper = JacksonJson().objectMapper.registerKotlinModule()
    val deployerProject = repo.projects.values.firstOrNull { it.name == "deployer" }

    val deployer = Deployer(repo, telegram, GitlabService, Applications)
    DeployerHttp(deployer, repo, gitlabEventsMapper).createServer().start()

    log.info("deployer started")
    deployerProject?.telegram?.let { telegram.sendMessage(it, "deployer started") }
}

