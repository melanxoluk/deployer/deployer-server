package org.melanxoluk.deployer

import org.melanxoluk.deployer.gateways.Gitlab
import org.melanxoluk.deployer.gateways.events.PipelineEvent
import org.slf4j.LoggerFactory


object GitlabService {
    private val log = LoggerFactory.getLogger(GitlabService::class.java)


    fun downloadArtifact(project: DeployerProject, pipeline: PipelineEvent): ByteArray {
        val gitlabProjectId = project.project.id
        val jobId = pipeline.builds?.get(0)?.id
        return Gitlab(project.gitlab.token).downloadArtifact(gitlabProjectId.toLong(), jobId!!)
    }
}