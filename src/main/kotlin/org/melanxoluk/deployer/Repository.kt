package org.melanxoluk.deployer

import com.typesafe.config.ConfigFactory
import org.gitlab4j.api.GitLabApi
import org.gitlab4j.api.models.Project
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.selectAll
import org.jetbrains.exposed.sql.transactions.transaction
import org.melanxoluk.deployer.entities.Deploy
import org.melanxoluk.deployer.entities.DeployerProjects
import org.melanxoluk.deployer.entities.GitlabServices
import org.melanxoluk.deployer.entities.TelegramChannels
import org.slf4j.LoggerFactory
import java.io.File
import java.nio.file.Files
import java.nio.file.Paths


class Repository {
    val gitlabServices = mutableMapOf<String, GitlabToken>()
    val telegramChannels = mutableMapOf<String, TelegramChannel>()
    val projects = mutableMapOf<String, DeployerProject>()

    init {
        val config = ConfigFactory.parseFile(File("deployer.conf"))
        val configObject = config.getObject("db")
        val database = configObject["database"]?.unwrapped().toString()
        val host = configObject["host"]?.unwrapped().toString()
        val port = configObject["port"]?.unwrapped().toString()
        val user = configObject["user"]?.unwrapped().toString()
        val password = configObject["password"]?.unwrapped().toString()

        Database.connect(
            url = "jdbc:postgresql://$host:$port/$database",
            driver = "org.postgresql.Driver",
            user = user,
            password = password)

        transaction {
            SchemaUtils.create(
                GitlabServices,
                TelegramChannels,
                DeployerProjects,
                Deploy
            )
        }

        log.debug("repository inited")
    }

    fun reload() {
        gitlabServices.clear()
        telegramChannels.clear()
        projects.clear()

        transaction {
            GitlabServices.selectAll().map {
                val name = it[GitlabServices.name]
                gitlabServices[name] =
                    GitlabToken(
                        name,
                        it[GitlabServices.host],
                        it[GitlabServices.token]
                    )
            }

            TelegramChannels.selectAll().map {
                val name = it[TelegramChannels.name]
                telegramChannels[name] =
                    TelegramChannel(
                        name,
                        it[TelegramChannels.token],
                        it[TelegramChannels.channel]
                    )
            }

            DeployerProjects.leftJoin(GitlabServices).leftJoin(TelegramChannels).selectAll().map {
                val gitlabService = gitlabServices[it[GitlabServices.name]]!!
                val gitlabPath = it[DeployerProjects.gitlabPath]
                val gitlabProject = getProject(gitlabPath, gitlabService)

                projects[it[DeployerProjects.key]] =
                    DeployerProject(
                        it[DeployerProjects.id],
                        it[DeployerProjects.name],
                        it[DeployerProjects.port],
                        Paths.get(it[DeployerProjects.path]),
                        it[DeployerProjects.key],
                        gitlabService,
                        telegramChannels[it[TelegramChannels.name]]!!,
                        gitlabProject,
                        if (it[DeployerProjects.deployScript] != null) {
                            Paths.get(it[DeployerProjects.deployScript]!!)
                        } else null
                    )

                Files.createDirectories(projects[it[DeployerProjects.key]]!!.path)
            }
        }
    }

    private fun getProject(name: String, gitlabService: GitlabToken): Project {
        try {
            val gitlabApi = GitLabApi(gitlabService.host, gitlabService.token)
            return gitlabApi.projectApi.getProject(name)
        } catch (ex: Exception) {
            log.error("can't get project '$name' from gitlab", ex)
            throw ex;
        }
    }

    private companion object {
        private val log = LoggerFactory.getLogger(Repository::class.java)
    }
}

fun main(args: Array<String>) {
    val gitlabApi = GitLabApi("https://gitlab.com", "2sveFqHBiuQdev39nD7B")
    val project = gitlabApi.projectApi.getProject(14644219)
    println(project)
}
