package org.melanxoluk.deployer.install

import net.schmizz.sshj.SSHClient


/**
 * Проект - это набор файлов и папок. Кто управляет ими?
 * - filesystem
 * - minio
 * - s3
 */
object InstallDeployer {
    operator fun invoke(hostname: String, username: String, password: String) {
        newSession(hostname, username, password).use { session ->

        }
    }

    private fun newSession(hostname: String, username: String, password: String) =
        with(SSHClient()) {
            loadKnownHosts()
            connect(hostname)
            authPassword(username, password)
            startSession()
        }
}