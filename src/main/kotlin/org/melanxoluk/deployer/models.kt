package org.melanxoluk.deployer

import org.gitlab4j.api.models.Project
import java.nio.file.Path
import java.util.*


data class GitlabToken(
    val name: String,
    val host: String,
    val token: String)

data class DeployerProject(
    val id: UUID,
    val name: String,
    val port: Int,
    val path: Path,
    val key: String,
    val gitlab: GitlabToken,
    val telegram: TelegramChannel,
    val project: Project,
    val deployScript: Path? = null)

data class TelegramChannel(
    val name: String,
    val token: String,
    val channel: String)
