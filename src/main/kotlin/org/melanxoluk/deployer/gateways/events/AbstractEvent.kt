package org.melanxoluk.deployer.gateways.events

abstract class AbstractEvent : Event {
    override var requestUrl: String? = null
    override var requestQueryString: String? = null

    var secretToken: String? = null
    override var requestSecretToken: String?
        get() = secretToken
        set(value) {
            secretToken = value
        }
}