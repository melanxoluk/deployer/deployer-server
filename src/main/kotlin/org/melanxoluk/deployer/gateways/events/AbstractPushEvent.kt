package org.melanxoluk.deployer.gateways.events

import com.fasterxml.jackson.annotation.JsonIgnore
import org.gitlab4j.api.webhook.EventCommit
import org.gitlab4j.api.webhook.EventProject
import org.gitlab4j.api.webhook.EventRepository

abstract class AbstractPushEvent {
    var eventName: String? = null
        get() = field
    var after: String? = null
    var before: String? = null
    var ref: String? = null
    var checkoutSha: String? = null
    var userId: Int? = null
    var userName: String? = null
    var userUsername: String? = null
    var userEmail: String? = null
    var userAvatar: String? = null
    var projectId: Int? = null
    var project: EventProject? = null
    var repository: EventRepository? = null
    var commits: List<EventCommit>? = null
    var totalCommitsCount: Int? = null

    @get:JsonIgnore
    var requestUrl: String? = null
        get() = field

    @get:JsonIgnore
    var requestQueryString: String? = null
        get() = field

    @get:JsonIgnore
    var requestSecretToken: String? = null
        get() = field

    /**
     * Gets the branch name from the ref. Will return null if the ref does not start with "refs/heads/".
     *
     * @return the branch name from the ref
     */
    @get:JsonIgnore
    val branch: String?
        get() {
            var ref = ref
            if (ref == null || ref.trim { it <= ' ' }.length == 0) {
                return null
            }
            ref = ref.trim { it <= ' ' }
            val refsHeadsIndex: Int = ref.indexOf(AbstractPushEvent.Companion.REFS_HEADS)
            return if (refsHeadsIndex != 0) {
                null
            } else ref.substring(AbstractPushEvent.Companion.REFS_HEADS.length)
        }

    companion object {
        private const val REFS_HEADS = "refs/heads/"
    }
}