package org.melanxoluk.deployer.gateways.events

import org.gitlab4j.api.models.User
import org.gitlab4j.api.models.Variable
import org.gitlab4j.api.utils.JacksonJson
import org.gitlab4j.api.webhook.EventCommit
import org.gitlab4j.api.webhook.EventProject
import org.melanxoluk.deployer.gateways.models.Job
import java.util.*

class PipelineEvent : AbstractEvent() {
    var objectAttributes: ObjectAttributes? = null
    var user: User? = null
    var project: EventProject? = null
    var commit: EventCommit? = null
    var builds: List<Job>? = null

    override val objectKind: String?
        get() = OBJECT_KIND

    class ObjectAttributes {
        var id: Int? = null
        var ref: String? = null
        var tag: Boolean? = null
        var sha: String? = null
        var beforeSha: String? = null
        var source: String? = null
        var status: String? = null
        var stages: List<String>? = null
        var createdAt: Date? = null
        var finishedAt: Date? = null
        var duration: Int? = null
        var variables: List<Variable>? = null
    }

    override fun toString(): String {
        return JacksonJson.toJsonString(this)
    }

    companion object {
        const val X_GITLAB_EVENT = "Pipeline Hook"
        const val OBJECT_KIND = "pipeline"
    }
}