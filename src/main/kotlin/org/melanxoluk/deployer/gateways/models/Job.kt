package org.melanxoluk.deployer.gateways.models

import org.gitlab4j.api.models.*
import org.gitlab4j.api.utils.JacksonJson
import java.util.*

class Job {
    var id: Long? = null
    var commit: Commit? = null
    var coverage: String? = null
    var createdAt: Date? = null
    var finishedAt: Date? = null
    var artifactsExpireAt: Date? = null
    var name: String? = null
    var pipeline: Pipeline? = null
    var ref: String? = null
    var runner: Runner? = null
    var user: User? = null
    var startedAt: Date? = null
    var artifactsFile: ArtifactsFile? = null
    var artifacts: List<Artifact>? = null
    var tag: Boolean? = null
    var webUrl: String? = null
    var stage: String? = null
    var status: JobStatus? = null
    var `when`: String? = null
    var manual: Boolean? = null
    var allowFailure: Boolean? = null
    var duration: Float? = null
    var project: Project? = null

    fun withId(id: Long?): Job {
        this.id = id
        return this
    }

    fun withCommit(commit: Commit?): Job {
        this.commit = commit
        return this
    }

    fun withCoverage(coverage: String?): Job {
        this.coverage = coverage
        return this
    }

    fun withCreatedAt(createdAt: Date?): Job {
        this.createdAt = createdAt
        return this
    }

    fun withFinishedAt(finishedAt: Date?): Job {
        this.finishedAt = finishedAt
        return this
    }

    fun withName(name: String?): Job {
        this.name = name
        return this
    }

    fun withPipeline(pipeline: Pipeline?): Job {
        this.pipeline = pipeline
        return this
    }

    fun withRef(ref: String?): Job {
        this.ref = ref
        return this
    }

    fun withRunner(runner: Runner?): Job {
        this.runner = runner
        return this
    }

    fun withUser(user: User?): Job {
        this.user = user
        return this
    }

    fun withStartedAt(startedAt: Date?): Job {
        this.startedAt = startedAt
        return this
    }

    fun withArtifactsFile(artifactsFile: ArtifactsFile?): Job {
        this.artifactsFile = artifactsFile
        return this
    }

    fun withTag(tag: Boolean?): Job {
        this.tag = tag
        return this
    }

    fun withStage(stage: String?): Job {
        this.stage = stage
        return this
    }

    fun withStatus(status: JobStatus?): Job {
        this.status = status
        return this
    }

    fun withWhen(`when`: String?): Job {
        this.`when` = `when`
        return this
    }

    fun withManual(manual: Boolean?): Job {
        this.manual = manual
        return this
    }

    fun withAllowFailure(allowFailure: Boolean?): Job {
        this.allowFailure = allowFailure
        return this
    }

    fun withDuration(duration: Float?): Job {
        this.duration = duration
        return this
    }

    fun withProject(project: Project?): Job {
        this.project = project
        return this
    }

    override fun toString(): String {
        return JacksonJson.toJsonString(this)
    }
}