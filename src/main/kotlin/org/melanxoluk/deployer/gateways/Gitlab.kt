package org.melanxoluk.deployer.gateways

import org.http4k.client.JavaHttpClient
import org.http4k.core.HttpHandler
import org.http4k.core.Method
import org.http4k.core.Request
import java.net.http.HttpClient

class Gitlab(private val accessToken: String) {
    fun downloadArtifact(projectId: Long, jobId: Long): ByteArray {
        val request =
            Request(
                Method.GET,
                "https://gitlab.com/api/v4/projects/${projectId}/jobs/${jobId}/artifacts?access_token=${accessToken}")

        val client: HttpHandler =
            JavaHttpClient(
                HttpClient.newBuilder()
                    .version(HttpClient.Version.HTTP_1_1)
                    .followRedirects(HttpClient.Redirect.NORMAL)
                    .build()
            )

        return client(request).body.stream.readAllBytes()
    }
}