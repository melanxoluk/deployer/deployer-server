package org.melanxoluk.deployer

import com.pengrad.telegrambot.TelegramBot
import com.pengrad.telegrambot.model.request.ParseMode
import com.pengrad.telegrambot.request.SendMessage


class Telegram {
    fun sendMessage(telegramChannel: TelegramChannel, msg: String) =
        with(telegramChannel) {
            TelegramBot(token).execute(
                SendMessage(channel, msg)
                    .parseMode(ParseMode.Markdown)
                    .disableWebPagePreview(true)
            )
        }
}
