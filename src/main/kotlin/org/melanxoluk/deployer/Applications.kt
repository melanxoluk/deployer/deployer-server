package org.melanxoluk.deployer

import org.jetbrains.exposed.sql.insert
import org.jetbrains.exposed.sql.transactions.transaction
import org.melanxoluk.deployer.entities.Deploy
import org.slf4j.LoggerFactory


object Applications {
    private val log = LoggerFactory.getLogger(Applications::class.java)


    fun deployDeployer(project: DeployerProject) {
        log.info("self deploy started")

        ProcessBuilder(
            "nohup",
            "sh",
            "/root/deployer/swap.sh",
            project.path.toAbsolutePath().toString(),
            project.name,
            project.port.toString()
        ).start()
    }

    fun deployProject(project: DeployerProject) {
        val processBuilder = ProcessBuilder(
                "sh",
                "/root/deployer/swap.sh",
                project.path.toAbsolutePath().toString(),
                project.name,
                project.port.toString()
            )

        val processResults = runProcess(processBuilder)

        transaction {
            Deploy.insert {
                it[projectId] = project.id
                it[buildOutput] = processResults.output
                it[buildExitValue] = processResults.exitValue
            }
        }

        log.info("deploy finished with code ${processResults.exitValue}")
    }

    fun deployStatic(project: DeployerProject) {
        val scriptPath =
            project.deployScript?.toAbsolutePath()?.toString()
                ?: "/root/deployer/static_swap.sh"

        val builder = ProcessBuilder(
            "sh",
            scriptPath,
            project.path.toAbsolutePath().toString(),
            project.name
        )

        val processResults = runProcess(builder)

        transaction {
            Deploy.insert {
                it[projectId] = project.id
                it[buildOutput] = processResults.output
                it[buildExitValue] = processResults.exitValue
            }
        }

        log.info("deploy static finished with code ${processResults.exitValue}")
    }
}
