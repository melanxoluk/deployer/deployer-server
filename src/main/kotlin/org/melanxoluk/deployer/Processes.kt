package org.melanxoluk.deployer

import java.nio.file.Files

data class ProcessResults(
    val exitValue: Int,
    val output: String
)

fun runProcess(pb: ProcessBuilder): ProcessResults {
    val outputFile = Files.createTempFile("deployer", "output")
    pb.redirectOutput(outputFile.toFile())
    pb.redirectError(outputFile.toFile())

    val exitValue = pb.start().run {
        waitFor()
        exitValue()
    }

    return ProcessResults(exitValue, String(Files.readAllBytes(outputFile)))
}