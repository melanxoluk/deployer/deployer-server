package org.melanxoluk.deployer

import org.melanxoluk.deployer.gateways.events.Event
import org.melanxoluk.deployer.gateways.events.PipelineEvent
import org.slf4j.LoggerFactory
import java.nio.file.Files


class Deployer(
    private val repo: Repository,
    private val telegram: Telegram,
    private val gitlab: GitlabService,
    private val apps: Applications) {

    fun deploy(project: DeployerProject, event: Event) {
        val pipeline = shouldDeploy(event) ?: return

        log.info("start to deploy project ${project.name}")

        val artifactBytes =
            gitlab.downloadArtifact(
                project,
                pipeline)

        log.info("artifact bytes are downloaded")

        val path = project.path.resolve("artifacts.zip")
        Files.write(path, artifactBytes)
        log.info("artifacts.zip downloaded to $path")

        // deployer should update itself on background
        if (project.name == "deployer") {
            apps.deployDeployer(project)
            // while any other could run in foreground
        } else {
            if (project.port == 0) {
                apps.deployStatic(project)
            } else {
                apps.deployProject(project)
            }
        }

        log.info("project ${project.name} deployed")

        telegram.sendMessage(project.telegram, "deploy swap started for **${project.name}**")
    }

    private fun shouldDeploy(event: Event): PipelineEvent? {
        if (event.objectKind != PipelineEvent.OBJECT_KIND) return null

        val pipeline = event as PipelineEvent
        val status = pipeline.objectAttributes?.status
        return if (status == "success") pipeline else null
    }


    fun reload() {
        repo.reload()
    }


    private companion object {
        private val log = LoggerFactory.getLogger(Deployer::class.java)
    }
}