insert into telegram_channels
values ('06afa828-9bd2-4cb0-8f7b-07b69e5b8de4',
        'melanxoluk',
        '939457300:AAGZJjfoK3aNiMSy8_NNkp0PVtK0H2SbZZE',
        '-1001390612129');

insert into telegram_channels (id, name, token, channel)
values ('9ad27c64-e574-4083-9171-743a06196d5f',
        'leandi',
        '888975955:AAFdogGiGK3aCqjxameIFBn8R55_GWuXw3s',
        '-1001344797591');

insert into gitlab_services (id, name, host, token)
values ('86107ede-322a-4e99-8a91-7f0f2cb22b86',
        'melanxoluk',
        'https://gitlab.com',
        '2sveFqHBiuQdev39nD7B');

insert into deployer_projects (id, name, port, path, key, gitlab_id, gitlab_path, telegram_id)
values ('b1e26aa2-685c-461d-b18e-f4f229249141',
        'deployer',
        '11001',
        '/root/deployer',
        '1',
        '86107ede-322a-4e99-8a91-7f0f2cb22b86',
        'melanxoluk/deployer/deployer-server',
        '06afa828-9bd2-4cb0-8f7b-07b69e5b8de4');

insert into deployer_projects (id, name, port, path, key, gitlab_id, gitlab_path, telegram_id)
values ('93da3ad6-7474-4d7c-9189-1307fba29e64',
        'modeler-parent',
        '0',
        '/root/leandi/modeler/modeler-parent',
        '20',
        '86107ede-322a-4e99-8a91-7f0f2cb22b86',
        'leandi/modeler/modeler-parent',
        '9ad27c64-e574-4083-9171-743a06196d5f');

insert into deployer_projects (id, name, port, path, key, gitlab_id, gitlab_path, telegram_id)
values ('46617a23-36de-48fe-8c4a-c41b9ececc81',
        'modeler-server',
        '20000',
        '/root/leandi/modeler/modeler-server',
        '21',
        '86107ede-322a-4e99-8a91-7f0f2cb22b86',
        'leandi/modeler/modeler-server',
        '9ad27c64-e574-4083-9171-743a06196d5f');

-- projects.melanxoluk.ru

insert into deployer_projects (id, name, port, path, key, gitlab_id, gitlab_path, telegram_id, deploy_script_path)
values ('1c597095-c989-4d2a-a520-3d1f2f12e0e1',
        'projects-data-model',
        '0',
        '/root/melanxoluk/projects/projects-data-model',
        '40',
        '86107ede-322a-4e99-8a91-7f0f2cb22b86',
        'melanxoluk/projects/projects-data-model',
        'ce68cd28-fc7f-455a-b5f9-f3ce59206c14',
        '/root/deployer/compose.sh');

insert into deployer_projects (id, name, port, path, key, gitlab_id, gitlab_path, telegram_id)
values ('51aeb2ee-33e3-44b5-a23d-eb1e3d284c89',
        'projects-api',
        '80',
        '/root/melanxoluk/projects/projects-api',
        '41',
        '86107ede-322a-4e99-8a91-7f0f2cb22b86',
        'melanxoluk/projects/projects-api',
        'ce68cd28-fc7f-455a-b5f9-f3ce59206c14');


  /*{
    name: "modeler-server",
    port: 20000,
    path: "/root/leandi/modeler/modeler-server",
    key: "21",
    gitlab: "melanxoluk"
    project_name: "leandi/modeler/modeler-server",
    telegram: "leandi"
  }*/

/*
gitlab: [
  {
    name = "melanxoluk"
    host = "https://gitlab.com"
    token = "2sveFqHBiuQdev39nD7B"
  }
]

projects: [
  {
    name: "deployer",
    port: 11001,
    path: "/root/deployer",
    key: "1",
    gitlab: "melanxoluk"
    project_name: "melanxoluk/deployer/deployer-server",
    telegram: "melanxoluk"
  }
  {
    name: "ignis",
    port: 11000,
    path: "/root/ignis",
    key: "2",
    gitlab: "melanxoluk"
    project_name: "melanxoluk/ignis-fatuus",
    telegram: "melanxoluk"
  }
  {
    name: "artifacts",
    port: 14000,
    path: "/root/artifacts",
    key: "6",
    gitlab: "melanxoluk"
    project_name: "melanxoluk/artifacts",
    telegram: "melanxoluk"
  }
  {
    name: "infra",
    port: 15000,
    path: "/root/infra",
    key: "7",
    gitlab: "melanxoluk"
    project_name: "melanxoluk/infra",
    telegram: "melanxoluk"
  }
  {
    name: "incubator-parent",
    port: 0,
    path: "/root/incubator/parent",
    key: "8",
    gitlab: "melanxoluk"
    project_name: "melanxoluk/incubator/incubator-parent",
    telegram: "melanxoluk",
    deploy_script: "compose.sh"
  }
  {
    name: "modeler-parent",
    port: 0,
    path: "/root/leandi/modeler/modeler-parent",
    key: "20",
    gitlab: "melanxoluk"
    project_name: "leandi/modeler/modeler-parent",
    telegram: "leandi",
    deploy_script: "compose.sh"
  }
  {
    name: "modeler-server",
    port: 20000,
    path: "/root/leandi/modeler/modeler-server",
    key: "21",
    gitlab: "melanxoluk"
    project_name: "leandi/modeler/modeler-server",
    telegram: "leandi"
  }
  {
    name: "modeler-client",
    port: 0,
    path: "/root/leandi/modeler/modeler-client",
    key: "22",
    gitlab: "melanxoluk"
    project_name: "leandi/modeler/modeler-client",
    telegram: "leandi",
    deploy_script: "static_swap.sh"
  }
]*/